# group 2
Xinao Han(Xinao.Han@campus.lmu.de) & Jing Wang(Jing.Wang1@campus.lmu.de) & Wei Xia (W.Xia1@campus.lmu.de)




# Unsupervised Multimodal Learning in Hate Speech Detection


## Description
In this work, we propose **Hate Speech Detection with Pseudo-labels (HSDp)**, which is an unsupervised framework based on deep clustering learning, to test the effectiveness of unsupervised multimodal on hate speech detection tasks. We also deploy a supervised model as our baseline.



## Installation
* python
* sklearn
* torch
* transformer
* csv
* numpy

## Data, Pretrained Model/Embeddings
We use 
1. [MultiOFF dataset](https://drive.google.com/drive/folders/1hKLOtpVmF45IoBmJPwojgq6XraLtHmV6)
2. Bert as encoder for text
3. [ResNet](https://download.pytorch.org/models/resnet152-394f9c45.pth) as encoder for image


## Supervised Baseline

This project establishes a baseline using supervised learning to evaluate classifier performance with pre-trained embeddings and gold labels. The key aspects are:


### Training Setup

- **Optimizer:** Adam with a learning rate of $2 \times 10^{-5}$.
- **Batch Size:** 32
- **Epochs:** 10
- **Configurations:** Evaluated across twelve setups, varying task types and freezing settings.
- **Loss Functions:** Cross-entropy and log cosh, providing insights into the model's learning and robustness to outliers.



## Usage
* run clustering.ipynb for clustering only baseline.
* run New_supervised_Xia.ipynb for supervised baseline with cross-entropy.
* run New_log_cosh_supervised_Xia.ipynb for supervised baseline with log cosh.

To replicate the experiments, please download the [dataset](https://drive.google.com/drive/folders/1hKLOtpVmF45IoBmJPwojgq6XraLtHmV6) and [ResNet](https://download.pytorch.org/models/resnet152-394f9c45.pth) to your file path first. Please change the file name of ResNet to "resnet101.pth" and also modify the paths in the codes correspondingly.